import groovy.io.FileType
import java.nio.file.Paths

def modulePathString = "C:\\Users\\mmckay01.MIR\\CNDA\\Modules\\nrg_webapp_xnatcalendar"
def pluginPathString = "C:\\Users\\mmckay01.MIR\\CNDA\\Plugins2\\nrg_webapp_xnatcalendar"
def pluginFullName = "XNAT 1.7 Calendar Plugin"
def pluginClassName = "CalendarPlugin"
def pluginDescription = "This is the XNAT 1.7 Calendar Plugin."

def parseJava(path) {
	if(path.endsWith(".java")){
		def javaFile = new File(path);
		javaFileText = javaFile.text;
		if(javaFileText.contains("org.nrg.xnat.workflow.PipelineEmailHandlerAbst")){
			println "In $path you are referencing the old location of PipelineEmailHandlerAbst. To get your class to work properly, you should change org.nrg.xnat.workflow.PipelineEmailHandlerAbst to org.nrg.xnat.event.listeners.PipelineEmailHandlerAbst\n"
		}
		if(javaFileText.contains("org.nrg.xft.event.Event")){
			println "In $path you are referencing the old version of the XFT event class. To get your class to work properly, you should change org.nrg.xft.event.Event to org.nrg.xft.event.entities.WorkflowStatusEvent"
			if(javaFileText.contains("public void handleEvent")){
				println "You will also need to update your handleEvent method in $path. You should change the method parameters from Event and WrkWorkflowdata to just WorkflowStatusEvent (i.e handleEvent(WorkflowStatusEvent e)). You will now want to get the workflow from the event by adding the following code to the beginning of your handleEvent method:"
				println "if (!(e.getWorkflow() instanceof WrkWorkflowdata)) {return;}"
				println "WrkWorkflowdata wrk = (WrkWorkflowdata)e.getWorkflow();\n"
			}
		}
		if(javaFileText.contains("GetArchiveRootPath")){
			println "In $path you are referencing the GetArchiveRootPath method. The name of that method has changed and you should no longer capitalize the G (so you would reference it as getArchiveRootPath).\n"
		}
		if(javaFileText.contains("implements DicomProjectIdentifier") && !javaFileText.contains("UserI")){
			println "The DicomProjectIdentifier interface has changed to now have the user parameter in the apply method be a UserI rather than an XDATUser. You should update $path to use org.nrg.xft.security.UserI since it implements DicomProjectIdentifier.\n"
		}
		if(javaFileText.contains("TurbineUtils.getUser(")){
			println "In $path you appear to be using the deprecated syntax to get the user object. You should instead get the user object with XDAT.getUserDetails(). Also, in earlier versions of XNAT, the user object returned was a XDATUser. It now returns a UserI, so you should modify your code as needed.\n"
		}
		if(javaFileText.contains(".getSecret()")){
			println "In $path you appear to be using the getSecret method on an alias token. As of XNAT 1.7, the secret is now a String rather than a long. If necessary, you should update how you are using it.\n"
		}
		if(javaFileText.contains("extends SecureResource") && javaFileText.contains("user")){
			println "In $path it looks like you might be trying to reference the user object from SecureResource. In XNAT 1.7, the user object is now private and if you want to use it in classes that extend SecureResource, you will need to do getUser() to obtain it.\n"
		}
		if(javaFileText.contains("siteUrl") || javaFileText.contains("SiteUrl") || javaFileText.contains("SiteURL")){
			println "In $path it looks like you are trying to get the site URL. As of XNAT 1.7, this should be accessed via XDAT.getSiteConfigPreferences().getSiteUrl()\n"
		}
		if(javaFileText.contains(".canRead(")){
			println "In $path it looks like you are doing a permissions check. Your old permissions code may no longer work and you may want to use the Permissions class (e.g. Permissions.canRead(userObject, projectObject) to check if a user can read a project).\n"
		}
		if(javaFileText.contains("@Service") || javaFileText.contains("@Repository") || javaFileText.contains("@XapiRestController") || javaFileText.contains("@RestController") || javaFileText.contains("@Controller") || javaFileText.contains("@NrgPreferenceBean")){
			println "You have a component-based annotation on $path. In order for Spring to register your components, you should add a @ComponentScan annotation to the generated plugin class (e.g. @ComponentScan({\"org.nrg.xnat.workshop.subjectmapping.services.impl\",\"org.nrg.xnat.workshop.subjectmapping.repositories\") ).\n"
		}
		if(javaFileText.contains("Entity")){
			println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
		if(javaFileText.contains("AdminUtils.getAdminEmailId")){
			println "In $path you should update how you get the admin email address. There is no longer an AdminUtils.getAdminEmailId() method and you should use XDAT.getSiteConfigPreferences().getAdminEmail() or XDAT.getNotificationsPreferences().getHelpContactInfo() instead.\n"
		}
		if(javaFileText.contains("AdminUtils.getMailServer")){
			println "In XNAT 1.7, you can no longer get the mail server via AdminUtils; you should replace AdminUtils.getMailServer() with XDAT.getNotificationsPreferences().getSmtpHostname(). The old syntax was found in $path.\n"
		}
		if(javaFileText.contains("XFT")){
			if(javaFileText.contains("XFT.GetSiteID") || javaFileText.contains("XFT.SetSiteID")){
				println "In XNAT 1.7, you can no longer get and set the site ID via XFT; you should replace XFT.GetSiteID() and XFT.SetSiteID(id) with XDAT.getSiteConfigPreferences().getSiteId() and XDAT.getSiteConfigPreferences().setSiteId(id). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetSiteURL") || javaFileText.contains("XFT.SetSiteURL")){
				println "In XNAT 1.7, you can no longer get and set the site URL via XFT; you should replace XFT.GetSiteURL() and XFT.SetSiteURL(url) with XDAT.getSiteConfigPreferences().getSiteUrl() and XDAT.getSiteConfigPreferences().setSiteUrl(url). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetAdminEmail") || javaFileText.contains("XFT.SetAdminEmail")){
				println "In XNAT 1.7, you can no longer get and set the admin email via XFT; you should replace XFT.GetAdminEmail() and XFT.SetAdminEmail(email) with XDAT.getSiteConfigPreferences().getAdminEmail() and XDAT.getSiteConfigPreferences().setAdminEmail(email). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetAdminEmailHost") || javaFileText.contains("XFT.SetAdminEmailHost")){
				println "In XNAT 1.7, you can no longer get and set the admin email host via XFT; you should replace XFT.GetAdminEmailHost() and XFT.SetAdminEmailHost(host) with XDAT.getNotificationsPreferences().getSmtpHostname() and XDAT.getNotificationsPreferences().getSmtpHostname(host). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetArchiveRootPath") || javaFileText.contains("XFT.SetArchiveRootPath")){
				println "In XNAT 1.7, you can no longer get and set the archive root path via XFT; you should replace XFT.GetArchiveRootPath() and XFT.SetArchiveRootPath(path) with XDAT.getSiteConfigPreferences().getArchivePath() and XDAT.getSiteConfigPreferences().setArchivePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetPrearchivePath") || javaFileText.contains("XFT.SetPrearchivePath")){
				println "In XNAT 1.7, you can no longer get and set the prearchive path via XFT; you should replace XFT.GetPrearchivePath() and XFT.SetPrearchivePath(path) with XDAT.getSiteConfigPreferences().getArchivePath() and XDAT.getSiteConfigPreferences().setArchivePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetCachePath") || javaFileText.contains("XFT.SetCachePath")){
				println "In XNAT 1.7, you can no longer get and set the cache path via XFT; you should replace XFT.GetCachePath() and XFT.SetCachePath(path) with XDAT.getSiteConfigPreferences().getCachePath() and XDAT.getSiteConfigPreferences().setCachePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetPipelinePath") || javaFileText.contains("XFT.SetPipelinePath")){
				println "In XNAT 1.7, you can no longer get and set the pipeline path via XFT; you should replace XFT.GetPipelinePath() and XFT.SetPipelinePath(path) with XDAT.getSiteConfigPreferences().getPipelinePath() and XDAT.getSiteConfigPreferences().setPipelinePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetRequireLogin") || javaFileText.contains("XFT.SetRequireLogin")){
				println "In XNAT 1.7, you can no longer get and set whether login is required via XFT; you should replace XFT.GetRequireLogin() and XFT.SetRequireLogin(isRequired) with XDAT.getSiteConfigPreferences().getRequireLogin() and XDAT.getSiteConfigPreferences().setRequireLogin(isRequired). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetEmailVerification") || javaFileText.contains("XFT.SetEmailVerification")){
				println "In XNAT 1.7, you can no longer get and set whether email verification is required via XFT; you should replace XFT.GetEmailVerification() and XFT.SetEmailVerification(verificationRequired) with XDAT.getSiteConfigPreferences().getEmailVerification() and XDAT.getSiteConfigPreferences().setEmailVerification(verificationRequired). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetUserRegistration") || javaFileText.contains("XFT.SetUserRegistration")){
				println "In XNAT 1.7, you can no longer get and set whether users are auto-approved via XFT; you should replace XFT.GetUserRegistration() and XFT.SetUserRegistration(autoApprove) with XDAT.getSiteConfigPreferences().getUserRegistration() and XDAT.getSiteConfigPreferences().getUserRegistration(autoApprove). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetEnableCsrfToken") || javaFileText.contains("XFT.SetEnableCsrfToken")){
				println "In XNAT 1.7, you can no longer get and set whether the CSRF token is enabled via XFT; you should replace XFT.GetEnableCsrfToken() and XFT.SetEnableCsrfToken(enable) with XDAT.getSiteConfigPreferences().getEnableCsrfToken() and XDAT.getSiteConfigPreferences().setEnableCsrfToken(enable). The old syntax was found in $path.\n"
			}
		}
		
		
		
		
		if(javaFileText.contains("AdminUtils.getMailServer()")){
				println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
		if(javaFileText.contains("AdminUtils.getAdminEmailId()")){
				println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
	}
}
println ""

//First we move each directory of files from the module to the corresponding location it should be in the plugin
moveFilesToPlugin(modulePathString,pluginPathString,["src","java"], ["src","main","java"],this.&parseJava)
moveFilesToPlugin(modulePathString,pluginPathString,["src","schemas"], ["src","main","resources","schemas"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","templates"], ["src","main","resources","META-INF","resources","templates"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","xnat-templates"], ["src","main","resources","META-INF","resources","xnat-templates"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","xdat-templates"], ["src","main","resources","META-INF","resources","xdat-templates"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","scripts"], ["src","main","resources","META-INF","resources","scripts"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","style"], ["src","main","resources","META-INF","resources","style"])
moveFilesToPlugin(modulePathString,pluginPathString,["src","images"], ["src","main","resources","META-INF","resources","images"])

//Next we copy in the gradle files
def currDir = new File(getClass().protectionDomain.codeSource.location.path).parent
moveFilesToPlugin(currDir,pluginPathString,["gradle"], ["gradle"])
try{
	def gradlewB = new File(Paths.get(currDir, "build.gradle").toString()).newDataInputStream()
	def gradlewDestB = new File(Paths.get(pluginPathString, "build.gradle").toString()).newDataOutputStream()
	gradlewDestB << gradlewB
	gradlewB.close()
	gradlewDestB.close()
}
catch(Throwable e){

}
try{
	def gradlew = new File(Paths.get(currDir, "gradlew").toString()).newDataInputStream()
	def gradlewDest = new File(Paths.get(pluginPathString, "gradlew").toString()).newDataOutputStream()
	gradlewDest << gradlew
	gradlew.close()
	gradlewDest.close()
}
catch(Throwable e){

}
try{	
	def gradlewBat = new File(Paths.get(currDir, "gradlew.bat").toString()).newDataInputStream()
	def gradlewBatDest = new File(Paths.get(pluginPathString, "gradlew.bat").toString()).newDataOutputStream()
	gradlewBatDest << gradlewBat
	gradlewBat.close()
	gradlewBatDest.close()
}
catch(Throwable e){

}
		  
//Then we create a settings.gradle file, using the name of the destination directory as the rootProject name
def pluginPath = Paths.get(pluginPathString)
def pluginDirName = pluginPath.getName(pluginPath.getNameCount() - 1)
new File(Paths.get(pluginPathString,"settings.gradle").toString()).withWriter { out ->
      out.println "rootProject.name = '"+pluginDirName+"'"
    }	

//We then create a plugin class for the plugin so that XNAT treats it as an XNAT plugin	
def classFileNameWithExtension = pluginClassName + ".java"
def pluginClassFile = new File(Paths.get(pluginPathString,"src","main","java","org","nrg","xnat","generated","plugin",classFileNameWithExtension).toString())
if(pluginClassFile.getParentFile()!=null){
		pluginClassFile.getParentFile().mkdirs()
	  }
def ls = System.getProperty("line.separator")
pluginClassFile.append("package org.nrg.xnat.generated.plugin;")
pluginClassFile.append(ls)
pluginClassFile.append(ls+"import org.nrg.framework.annotations.XnatDataModel;")
pluginClassFile.append(ls+"import org.nrg.framework.annotations.XnatPlugin;")
pluginClassFile.append(ls+"import org.springframework.context.annotation.Bean;")
pluginClassFile.append(ls+"import org.springframework.context.annotation.ComponentScan;")
pluginClassFile.append(ls)
def pluginString = /@XnatPlugin(value = "/+ pluginDirName +/", name = "/ +pluginFullName+ /", description = "/ + pluginDescription + /")/
pluginClassFile.append(ls+pluginString)
pluginClassFile.append(ls+"public class "+pluginClassName+" {")
pluginClassFile.append(ls+"}")

/*
This method is used for moving all the files from a certain directory in the module to the corresponding directory in the plugin.
It takes the path to the module directory, the path to the plugin directory,
an array of the directories that need to be added to the module path to get to the path of the directory within the module that we want to copy,
and an array of the directories that need to be added to the plugin path to get to the path of the directory within the plugin that we want to copy those files to.
*/ 
static void moveFilesToPlugin(String modulePathString,String pluginPathString, ArrayList<String> sourceDirectoryPathWithinModule, ArrayList<String> destinationDirectoryPathWithinPlugin, Closure fileProcesser=null) {
      try{
		def fullStructureToSourceDir = sourceDirectoryPathWithinModule.clone()
		fullStructureToSourceDir.add(0,modulePathString)
		def fullStructureToDestinationDir = destinationDirectoryPathWithinPlugin.clone()
		fullStructureToDestinationDir.add(0,pluginPathString)
		
		def sourceDir = new File(Paths.get(*fullStructureToSourceDir).toString());
		def fileList = []

		sourceDir.eachFileRecurse(FileType.FILES) { file ->
		  fileList << file
		}

		fileList.each {
		  def filePath = it.path
		  if(fileProcesser!=null){
			fileProcesser(filePath)
		  }
		  def relativeFilePath = sourceDir.toURI().relativize(new File(it.path).toURI()).getPath()
		  
		  def src = new File(filePath).newDataInputStream()
		  def fullStructureToDestinationFile = fullStructureToDestinationDir.clone()
		  fullStructureToDestinationFile.add(relativeFilePath)
		  
		  def destFile = new File(Paths.get(*fullStructureToDestinationFile).toString())
		  
		  if(destFile.getParentFile()!=null){
			destFile.getParentFile().mkdirs()
		  }
		  def dest = destFile.newDataOutputStream()
		  dest << src
		  src.close()
		  dest.close()
		}
	}
	catch(Throwable e){
	}
   }  
